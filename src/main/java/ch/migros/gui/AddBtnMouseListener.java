package ch.migros.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

public class AddBtnMouseListener extends MouseAdapter{
    private JComponent c;
    public AddBtnMouseListener(JComponent c){
        this.c = c;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Pressable p = (Pressable)c;
        p.pressed();
    }

}
