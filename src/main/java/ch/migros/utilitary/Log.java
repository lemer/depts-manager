package ch.migros.utilitary;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

public class Log implements Comparable<Log>{
    private int logCount = 0;

    String name;
    Person who;
    double paid;
    Set<Person> toWhom;
    MyDate when;
    int id;

    public Log(Person who, double paid, Collection<Person> toWhom, MyDate when, int id){
        this("Untitled", who, paid, toWhom, when, id);
    }

    public Log(String name, Person who, double paid, Collection<Person> toWhom, MyDate when, int id){
        this.name = name;
        this.who = who;
        this.paid = paid;
        this.toWhom = new HashSet<>(toWhom);
        this.when = when;
        this.id = id;
    }

    public String name(){
        return name;
    }

    public Person who(){
        return who;
    }

    public double amount(){
        return paid;
    }

    public Set<Person> toWhom(){
        return toWhom;
    }

    public void setId(int id){
        this.id = id;
    }

    public boolean remove(Person p){
        if(toWhom.remove(p)){
            toWhom.add(new Person("Anonymous"));
            return true;
        }
        return false;
    }

    @Override
    public String toString(){
        return toString(1);
    }

    public String toString(int logsSize){
        StringBuilder strB = new StringBuilder();
        strB.append(String.format("%s. \"%s\" [%s] "+who+" paid "+paid+" for ", String.format("%1$"+((int)Math.log10(logsSize)+1)+"s", id+1), name, when));
        Iterator<Person> it = toWhom.iterator();
        while(it.hasNext()){
            strB.append(it.next());
            if(it.hasNext()){
                strB.append(", ");
            }
        }
        return strB.toString();
    }

    public void undo() {
        for(Person p : toWhom){
            if(p.name() != "Anonymous")
                p.paid(who, paid/toWhom.size());
        }
    }

    @Override
    public boolean equals(Object o){
        Log l = (Log)o;
        return who.equals(l.who) && paid == l.paid && toWhom.equals(l.toWhom) && when.equals(l.when) && this.id == l.id;
    }

    @Override
    public int hashCode(){
        return Objects.hash(who, paid, toWhom, when, id);
    }

    @Override
    public int compareTo(Log that) {
        int compare = when.compareTo(that.when);
        if(compare == 0){
            if(this.equals(that)){
                return 0;
            }else{
                return Integer.compare(this.id, that.id);
            }
        }
        return compare;
    }

}

