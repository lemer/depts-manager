package ch.migros.utilitary;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.channels.InterruptedByTimeoutException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import ch.migros.utilitary.InterruptedExecutionException;

public final class FileManager{
    private File file;
    private Logger logger;
    private MsgProvider msgs = new MsgProvider();

    public FileManager(File f){
        file = f;
    }

    public void save(){
        save("infos");
    }

    public void save(String fileName){
        StringBuilder strB = new StringBuilder();
        TreeSet<Log> logs = new TreeSet<>(logger.getLogs());
        strB.append("Avoid modifying the following file!\n\n");

        strB.append("People ("+logger.getPeople().size()+") :\n");
        int tempI = 0;
        for (Person p : logger.getPeople()) {
            ++tempI;
            strB.append(p.name()+(tempI==logger.getPeople().size()? "":", "));
        }
        strB.append("\n\n");
        StringBuilder strBDebts = new StringBuilder();
        int counter = 0;
        for(Person p : logger.getPeople()){
            if(!p.getOwes().isEmpty()){
                ++counter;
                strBDebts.append("  "+p+" ("+p.getOwes().size()+") :\n");
                p.getOwes().forEach((name, amount) ->{
                    strBDebts.append("    owes "+amount+" to "+name+"\n");
                });
            }
        }
        strB.append("Debts ("+counter+") :\n"+strBDebts.toString());

        strB.append("\n\nLogs ("+logs.size()+") :\n");
        for(Log l : logs){
            strB.append("  "+l+"\n");
        }

        try {
            Writer w;
            w = new OutputStreamWriter(new FileOutputStream(new File(fileName)));
            w.write(strB.toString());
            w.close();
            System.out.println(msgs.saveWasSuccessful(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Logger open() {
        try (Reader r = new InputStreamReader(new FileInputStream(file));){
            readUntil(r, '(');
            int peopleLength = toInt(readUntilExclude(r, ')'));
            readUntil(r, '\n');
            Set<String> peopleStr = new HashSet<>();
            for(int i=0; i<peopleLength; ++i){
                peopleStr.add(readNext(r)[0]);
            }
            Set<Person> people = new HashSet<>();
            for(String s : peopleStr){
                people.add(new Person(s));
            }

            Logger readLogger = new Logger(new HashSet<>(), people);

            readUntil(r, '(');
            int debtsLength = toInt(readUntilExclude(r, ')'));
            readUntil(r, '\n');
            for(int i=0; i<debtsLength; ++i){
                String name = readNext(r)[0];
                readUntil(r, '(');
                int owesLength = toInt(readUntilExclude(r, ')'));
                readUntil(r, '\n');
                for(int j=0; j<owesLength; ++j){
                    readNext(r); //owes
                    double amount = Double.parseDouble(""+readNext(r)[0]);
                    readNext(r); //to
                    String toWhom = readNext(r)[0];
                    readLogger.getPerson(name).getOwes().put(readLogger.getPerson(toWhom), amount);
                }
            }

            readUntil(r, '(');
            int logsLength = toInt(readUntilExclude(r, ')' ));
            int id = 0;
            for(int i=0; i<logsLength; ++i){
                readUntil(r,'\"');
                String logName = readUntilExclude(r, '\"');
                readUntil(r,'[');
                int[] dateComp = new int[3];

                for(int j=0; j<3; ++j){
                    dateComp[j] = toInt(readUntilExclude(r, '/', ']'));
                }

                MyDate date = new MyDate(2000+dateComp[2], dateComp[1], dateComp[0]);

                String name = readNext(r)[0]; readNext(r);
                Double amount = Double.parseDouble(readNext(r)[0]); readNext(r);
                Set<Person> names = new HashSet<>();

                boolean isMore = true;
                while(isMore){
                    String[] str = readNext(r);
                    names.add(readLogger.getPerson(str[0]));
                    if(!str[1].equals(","))
                        isMore = false;
                }
                readLogger.getLogs().add(new Log(logName, readLogger.getPerson(name), amount, names, date, id++));
            }

            logger = readLogger;
        }catch(IOException | InterruptedExecutionException e){
            System.err.println("A problem occured when reading the file. There was either no infos file or it was corrupt. A new clean file has been created.");
            logger = new Logger(new HashSet<>(), new HashSet<>());
            save();
        }
        return logger;
    }

    private int toInt(String s){
        return Integer.parseInt(s);
    }

    private String[] readNext(Reader r) throws IOException, InterruptedExecutionException {
        char InitName = readUntilNot(r, ' ');
        String str = (InitName+readUntil(r, new Character[]{' ',  ',', '\n'}));
        int length = str.length();
        return new String[]{str.substring(0, length-1), ""+str.charAt(length-1)};
    }

    private char readUntilNot(Reader r, char a) throws IOException, InterruptedExecutionException{
        char c;
        do{
            int g = r.read();
            if (g == -1){
                throw new InterruptedExecutionException("Wrong file format.");
            }
            c = (char)g;
        }while(c == a);
        return c;
    }

    private String readUntilExclude(Reader r, Character ... a) throws IOException, InterruptedExecutionException{
        String tr = readUntil(r, a);
        return tr.substring(0,tr.length()-1);
    }
    private String readUntil(Reader r, char a) throws IOException, InterruptedExecutionException{
        char c;
        StringBuilder strB = new StringBuilder();
        do{
            int g = r.read();
            if(g == -1){
                throw new InterruptedExecutionException("Wrong file format.");
            }
            c = (char)g;
            strB.append(c);
        }while(c != a);
        return strB.toString();
    }
    private String readUntil(Reader r, Character[] a) throws IOException, InterruptedExecutionException{
        char c;
        StringBuilder strB = new StringBuilder();
        do{
            int g = r.read();
            if(g == -1){
                throw new InterruptedExecutionException("Wrong file format.");
            }
            c = (char)g;
            strB.append(c);
        }while(!Arrays.asList(a).contains(c));
        return strB.toString();

    }
}

