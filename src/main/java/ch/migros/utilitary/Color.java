package ch.migros.utilitary;

public enum Color{
    RESET("\u001B[0m"),
    BLACK("\u001B[30m"),
    RED("\u001B[31m"),
    GREEN("\u001B[32m"),
    YELLOW("\u001B[33m"),
    BLUE("\u001B[34m"),
    PURPLE("\u001B[35m"),
    CYAN("\u001B[36m"),
    WHITE("\u001B[37m"),
    BOLD("\u001B[1m");

    private final String strRep;

    private Color(String str){
        strRep = str;
    }

    @Override
    public String toString(){
        return strRep;
    }

    public String applyTo(String str){
        return this.toString() + str + RESET;
    }
}
