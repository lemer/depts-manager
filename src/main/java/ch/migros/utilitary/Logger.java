package ch.migros.utilitary;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.Map;
import java.util.TreeSet;
import java.util.List;

import ch.migros.utilitary.Color;
import ch.migros.utilitary.InterruptedExecutionException;

public class Logger{
    private Set<Log> logs = new TreeSet<>();
    private Set<Person> people = new HashSet<>();
    private boolean hasChanged;
    private static MyDate currentDate = new MyDate();
    private static MsgProvider msgs = new MsgProvider();

    public Logger(){
        this(new HashSet<>(), new HashSet<>());
    }

    public Logger(Set<Log> logs, Set<Person> people){
        this.logs = new TreeSet<>(logs);
        this.people = new HashSet<>(people);
    }

    public boolean removeLog(Log l) {
        l.undo();
        hasChanged = true;
        boolean valid = logs.remove(l);
        int nid = 0;
        for(Log al : logs){
            al.setId(nid++);
        }
        return valid;
    }
    public void removePerson(String name) throws InterruptedExecutionException{
        Person p;
        p = getPerson(name);
        Iterator<Log> it = logs.iterator();
        while(it.hasNext()){
            Log next = it.next();
            if(next.who().equals(p)){
                it.remove();
            }else{
                next.remove(p);
            }
        }
        Iterator<Person> iter = people.iterator();
        while(iter.hasNext()){
            Person nextP = iter.next();
            if(nextP.equals(p)){
                iter.remove();
            }
            nextP.removePerson(p);
        }
        hasChanged = true;
    }

    public Set<Log> getLogs() {
        return logs;
    }

    public boolean hasChanged(){
        return hasChanged;
    }

    public void setChanged(boolean setTo){
        hasChanged = setTo;
    }

    public boolean log(String logName, String who, double paid, Collection<String> toWhom){
        if (logName.equals("")){
            logName = "Untitled";
        }
        double owedPerPerson = paid / (toWhom.size());
        if(!peopleContains(who) || ! peopleContains(toWhom)){
            System.out.println("this message should never display. If it does, cry a lot.");
            return false;
        }
        Person payer;
        try{
            payer = getPerson(who);

            Set<Person> owers = new HashSet<>();
            for(String name : toWhom){
                Person p = getPerson(name);
                owers.add(p);

                payer.paid(p, owedPerPerson);
            }

            hasChanged = true;
            return log(logName, payer, paid, owers, currentDate);
        }catch(InterruptedExecutionException e){
            System.out.println("This message should not display : "+e.getMessage());
            return false;}
    }

    private boolean log(String name, Person who, double paid, Set<Person> toWhom, MyDate when){
        boolean valid = logs.add(new Log(name, who, paid, toWhom, when, logs.size()));
        return valid;
    }

    public Map<Person, Double> getSumDebts(){
        Map<Person, Double> sum = new HashMap<>();
        for(Person p : people){
            sum.putIfAbsent(p, 0.);
            for(Map.Entry<Person, Double> e : p.getOwes().entrySet()){
                sum.computeIfPresent(e.getKey(), (k, v) -> v+e.getValue());
                sum.computeIfAbsent(e.getKey(), k -> e.getValue());
                sum.computeIfPresent(p, (k,v) -> v-e.getValue());
            }
        }
        return sum;
    }

    // Used for debug.
    public void printSumDebts(){
        getSumDebts().forEach((k, v) -> System.out.println(k+" : "+v));
    }

    public boolean printDebtsFor(Person p){
        return printDebtsFor(p, false);
    }

    public Map<Person, Map<Person, Double>> getDebts(){
        Map<Person, Map<Person, Double>> toReturn = new HashMap<>();
        for(Person p : people){
            toReturn.put(p, p.getOwes());
        }
        return toReturn;
    }

    public boolean printDebtsFor(Person p, boolean printSum){
        StringBuilder strB = new StringBuilder();
        p.getOwes().forEach((k,v) -> strB.append(String.format("\tCHF%.2f to "+k+"\n",v)));

        String sumString = new String();
        if(printSum){
            Map<Person, Double> sum = getSumDebts();
            double amount = sum.get(p);
            StringBuilder sumStringB = new StringBuilder();
            sumStringB.append("(");
            if(amount >0){
                sumStringB.append(Color.GREEN.applyTo("+"));
            }else{
                sumStringB.append(Color.RED.applyTo("-"));
            }
            sumStringB.append(String.format("%.2f)", Math.abs(amount)));
            sumString = sumStringB.toString();
        }

        if (strB.length() == 0) {
            System.out.println(msgs.noDebts(p)+sumString);
        } else {
            System.out.print(p+" owes "+sumString+":\n"+strB.toString());
        }
        return true;
    }

    public void rearangeDebts(){
        List<Person> leftPeople = new LinkedList<>(people);

        Person p1;
        while(!leftPeople.isEmpty()){
            boolean done = true;
            p1 = leftPeople.get(0);
            System.out.println("While loop : looking at "+p1.name()+" size = "+leftPeople.size());
            System.out.println("p1 owes : "+p1.getOwes());

            Map<Person, Double> newP1Owes = new HashMap<>(p1.getOwes());
            // We check for all people p1 owes
            for(Map.Entry<Person, Double> p2Entry : p1.getOwes().entrySet()){
                Person p2 = p2Entry.getKey();
                Double p1ToP2 = p2Entry.getValue();
                Map<Person, Double> newP2Owes = new HashMap<>(p2.getOwes());
                System.out.println("======");
                System.out.println("p1 owes "+p2.name()+" "+p1ToP2+" and p2 owes to "+p2.getOwes().size()+" people.");
                System.out.println("p2 owes : "+p2.getOwes());

                // if this p2 also owes someting, then for all owed 3rd degree person
                if(!newP2Owes.isEmpty()){
                    done = false;

                    for(Map.Entry<Person, Double> p3Entry : p2.getOwes().entrySet()){
                        Person p3 = p3Entry.getKey();
                        Double p1ToP3;
                        Double p2ToP3 = p3Entry.getValue();
                        System.out.println("p2 owes "+p3.name()+" "+p2ToP3);

                        double difference = p1ToP2 - p2ToP3;
                        System.out.println("The difference is "+difference);

                        // if p1 owes less o p2 than p2 to p3 :
                        if(difference <= 0){
                            p1ToP3 = p1ToP2+newP1Owes.getOrDefault(p3, 0.);
                            p2ToP3 = -difference;
                            p1ToP2 = 0.;
                        }else{
                            p1ToP3 = p2ToP3+newP1Owes.getOrDefault(p3, 0.);
                            p1ToP2 = difference;
                            p2ToP3 = 0.;
                        }
                        System.out.println("p1 to p2 : "+p1ToP2+"   p2 to p3 : "+p2ToP3+"   p1 to p3 : "+p1ToP3);

                        newP1Owes.remove(p2);
                        newP2Owes.remove(p3);

                        if(p1ToP2 != 0){
                            newP1Owes.put(p2, p1ToP2);
                            System.out.println("keeping p1 to p2 "+p1ToP2);
                        }
                        if(p1ToP3 != 0 && !p1.equals(p3)){
                            newP1Owes.put(p3, p1ToP3);
                            System.out.println("keeping p1 to p3 "+p1ToP3);
                        }
                        if(p2ToP3 != 0){
                            newP2Owes.put(p3, p2ToP3);
                            System.out.println("keeping p2 to p3 "+p2ToP3);
                        }else{
                            System.out.println("p2 to p3 is zero, so we don't try other p3s");
                            break;
                        }
                    }
                    p1.getOwes().clear();
                    p1.getOwes().putAll(newP1Owes);
                    p2.getOwes().clear();
                    p2.getOwes().putAll(newP2Owes);
                    break;
                }
            }
            System.out.println("done with p1 ? "+done);
            if(done){
                System.out.println("Removing pfrom left people : "+p1.name());
                System.out.println("p1 now owes "+p1.getOwes());
                leftPeople.remove(p1);
            }
        }
    }

    public Set<Person> getPeople(){
        return people;
    }

    public Set<String> getNames(){
        Set<String> toReturn = new HashSet<>();
        for(Person p : people){
            toReturn.add(p.name());
        }
        return toReturn;
    }

    public Person getPerson(String name) throws InterruptedExecutionException{
        name = name.toLowerCase();
        for(Person p : people){
            if(p.name().toLowerCase().equals(name)){
                return p;
            }
        }
        throw new InterruptedExecutionException(msgs.personNotFound(name));
    }

    public boolean peopleContains(String name){
        for(Person p : people){
            if(p.name().toLowerCase().equals(name.toLowerCase())){
                return true;
            }
        }
        return false;
    }
    public boolean peopleContains(Collection<String> names){
        for(String n : names){
            if(!peopleContains(n)){
                return false;
            }
        }
        return true;
    }

    public boolean addPerson(String s) throws InterruptedExecutionException{
        if(s.toLowerCase().equals("anonymous")){
            throw new InterruptedExecutionException("Sorry, but this name is used by the program in some situations. Find yourself something more original.");
        }
        return addPerson(new Person(s));
    }
    public boolean addPerson(Person p){
        hasChanged = true;
        return people.add(p);
    }
}
