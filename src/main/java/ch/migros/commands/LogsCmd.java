package ch.migros.commands;

import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.migros.utilitary.Log;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.Logger;

public class LogsCmd extends Command {
    private Logger logger;

    public LogsCmd(Logger logger){
        super("logs");
        this.logger = logger;
    }

    @Override
    public boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) {
        // Get and print the people.
        if ( logger.getLogs().isEmpty()) {
            System.out.println(msgs.noLogsToList());
        }
        Set<Log> logs = logger.getLogs();
        int logsSize = logs.size();
        for(Log log: logs){
            System.out.println("\t"+log.toString(logsSize));
        }
        return true;
    }
}






