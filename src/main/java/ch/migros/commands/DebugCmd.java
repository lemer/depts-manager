package ch.migros.commands;

import java.util.List;
import java.util.Map;

import ch.migros.gui.GUIManager;
import ch.migros.utilitary.Interface;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.InterruptedExecutionException;
import ch.migros.utilitary.Logger;

public class DebugCmd extends Command{
    public static final String TITLE = "debug";

    private Interface i;
    private Logger logger;
    private static Option OPT_d = option("debts-arrange", TITLE);
    private static Option OPT_ds = option("debts-sum", "ds", TITLE);
    private static Option OPT_w = option("window", TITLE);

    public DebugCmd(Interface i, Logger logger) {
        super("debug", argsSizeEquals(0,1), true);
        addArgument("(on|off)");
        addOption(OPT_d);
        addOption(OPT_ds);
        addOption(OPT_w);
        this.logger = logger;
        this.i = i;;
    }

    public boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException{
        boolean debugMode;
        if(askedOptions.containsKey(OPT_w)){
            GUIManager gui = new GUIManager(logger);
            gui.openWindow();
            return true;
        }
        if(askedOptions.containsKey(OPT_d)){
            i.debugMode(true);
            i.getLogger().rearangeDebts();
            return true;
        }
        if(askedOptions.containsKey(OPT_ds)){
            i.getLogger().printSumDebts();
            return true;
        }
        if(args.size() == 0){
            debugMode = i.debugMode();
        }else{
            String arg = args.get(0);
            if(arg.equals("on")){
                i.debugMode(true);
                debugMode = true;
            }else if(arg.equals("off")){
                i.debugMode(false);
                debugMode = false;
            }else{
                throw new InterruptedExecutionException(msgs.expectedOnOff(arg));
            }

        }
        String fileName = debugMode ? ".debug" : i.getFileIn();
        System.out.println("Debug mode "+(debugMode? "enabled" : "disabled")+". All changes will be saved in '"+fileName+"' when quiting.");
        return true;
    }
}
