package ch.migros.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ch.migros.utilitary.InterruptedExecutionException;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.Color;

public final class Alias extends Command {
    public final Command aliasFor;
    public final List<String> args;

    public Alias(String title, Command aliasFor, List<String> args){
        super(title.toLowerCase(), p -> true, "Alias command for '"+aliasFor(aliasFor, args)+"', look up this command to get more information.", false);
        this.aliasFor = aliasFor;
        if (args == null){
            this.args = new ArrayList<>();
        }else{
            this.args = new ArrayList<>(args);
        }
        for(Option o : aliasFor.availableOptions){
            if(!availableOptions.contains(o)){
                addOption(o);
            }
        }
    }

    @Override
    protected boolean tryToRun(Map<Option, List<String>> askedOptions,
            List<String> args) throws InterruptedExecutionException {
        List<String> newArgs = new ArrayList<>();
        askedOptions.forEach( (k, v) -> {
                newArgs.add("-"+k.shortName());
                newArgs.addAll(v);
            } );
        newArgs.addAll(this.args);
        newArgs.addAll(args);
        return aliasFor.run(newArgs);
    }

    public static String aliasFor(Command aliasFor, List<String> args){
        StringBuilder b = new StringBuilder();
        b.append(aliasFor.title+" ");
        if(args != null)
            args.forEach(v -> b.append(v+" "));
        return b.substring(0, b.length()-1).toString();
    }

    @Override
    public void displayHelp(){
        StringBuilder strB = new StringBuilder();
        strB.append(Color.BOLD.applyTo(format())+(isHidden?" (hidden)":"")+"\n\t"+desc);
        System.out.println(strB.toString());
    }
}
