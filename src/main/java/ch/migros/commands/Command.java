package ch.migros.commands;

import java.util.Map;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import ch.migros.utilitary.Logger;
import ch.migros.utilitary.MsgProvider;
import ch.migros.utilitary.MyDate;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.Color;
import ch.migros.utilitary.InterruptedExecutionException;
import ch.migros.utilitary.Pair;


// TODO Add field for full description

public abstract class Command {
    public final String title;
    public final String desc;
    public final List<Pair<String, String>> arguments;

    protected final Predicate<List<String>> argsConditions;

    protected final MsgProvider msgs;
    protected final boolean isHidden;
    protected final Set<Option> availableOptions;

    public final static Map<String, Map<String, String>> helpInfos = getHelpInfos();
    protected final static Option OPT_h = new Option("help", helpInfos.get("_common").get("help"));
    public static final Scanner scan = new Scanner(System.in);
    protected static MyDate currentDate = new MyDate();

    // Constructors
    public Command(String title,  Predicate<List<String>> argsConditions, String desc, boolean isHidden){
        this.availableOptions = new HashSet<>();
        availableOptions.add(OPT_h);
        this.arguments = new ArrayList<>();
        this.argsConditions = argsConditions;
        this.title = title;
        this.desc = desc;
        this.msgs = new MsgProvider(this);
        this.isHidden = isHidden;
    }
    public Command(String title, Predicate<List<String>> argsConditions, boolean isHidden){
        this(title, argsConditions, helpInfos.get(title).get("desc"), isHidden);
    }
    public Command(String title, Predicate<List<String>> argsConditions){
        this(title, argsConditions, false);
    }
    public Command(String title, boolean isHidden){
        this(title, argsSizeEquals(0), isHidden);
    }
    public Command(String title){
        this(title, argsSizeEquals(0));
    }

    private String generateFormat(){
        StringBuilder strB = new StringBuilder();
        strB.append(title);
        for(Pair<String, String> p : arguments){
            strB.append(" "+p.getA());
        }
        return strB.toString();
    }

    public String format(){
        return generateFormat();
    }

    // Generates predicates for the number of arguments of a command.
    public static Predicate<List<String>> argsBetween(int a, int b){
        return l -> (l.size() < b && l.size() >= a);
    }
    public static Predicate<List<String>> argsSizeEquals(int ... a){
        return l -> {
                boolean valid = false;
                    for(int n : a){
                        if(l.size() == n){
                            valid = true;
                        }
                    }
                    return valid;
                };
    }
    public static Predicate<List<String>> argsMoreThan(int n){
        return l -> l.size()>=n;
    }

    public void addOption(Option o){
        availableOptions.add(o);
    }
    public void addArgument(String name, String desc){
        arguments.add(new Pair<String,String>(name, desc));
    }

    public void addArgument(String name){
        arguments.add(new Pair<String,String>(name,helpInfos.get(title).get(name)));
    }

    public static Option option(String name, String title){
        return new Option(name, helpInfos.get(title).get(name));
    }
    public static Option option(String name, String shortN, String title){
        return new Option(name, shortN, helpInfos.get(title).get(name));
    }
    public static Option option(String name, String shortName, String title, List<String> arguments){
        Map<String, String> args = new HashMap<>();
        for(String s : arguments){
            args.put(s, helpInfos.get(title).get(s));
        }
        return new Option(name, shortName, helpInfos.get(title).get(name), args);
    }

    // main run command, called from outside. This one tests the number of arguments and deals with exceptions from tryToRun
    public boolean run(List<String> args){
        try{
            Map<Option, List<String>> askedOptions = pullOptions(args);
            if(askedOptions.containsKey(OPT_h)){
                displayHelp();
                return true;
            }
            if(!argsConditions.test(args)){
                System.out.println(msgs.invalidNumberOfArgs());
                return false;
            }
            return tryToRun(askedOptions, args);
        } catch(InterruptedExecutionException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    // abstract function, the one that will implement de behavior of every command.
    // Throws InterruptedExecutionException with custom message explaining what stopped the execution of the command.
    // returns a boolean telling if an action has been done.
    protected abstract boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException;

    protected Double toDouble(String s) throws InterruptedExecutionException{
        try {
            Double d = Double.parseDouble(s);
            return d;
        } catch (NumberFormatException e) {
            throw new InterruptedExecutionException(msgs.isNotADouble(s));
        }
    }

    protected Map<Option, List<String>> pullOptions(List<String> args) throws InterruptedExecutionException{
        boolean noMore = false;
        int argsSize = args.size();
        Map<Option, List<String>> toReturn = new HashMap<>();

        if(argsSize == 0){
            return toReturn;
        }

        Iterator<String> it = args.iterator();

        while(it.hasNext()){
            String next = it.next();
            if(next.charAt(0) == '-'){
                if(noMore){
                    throw new InterruptedExecutionException(msgs.optionsAreNotFirst());
                }else{
                    //we want to modify args to have no more options
                    it.remove();
                    Set<Option> opts = getOption(next);
                    for(Option opt : opts){
                        if( opt != null){
                            int nbArg = opt.nbArgs();
                            List<String> arguments = new ArrayList<>();
                            for(int i=0; i<nbArg; ++i){
                                if(!it.hasNext()){
                                    throw new InterruptedExecutionException(msgs.invalidOptionFormat(opt));
                                }
                                arguments.add(it.next());
                                it.remove();
                            }
                            toReturn.put(opt, arguments);
                        }else{
                            throw new InterruptedExecutionException(msgs.unknownOption(next));
                        }
                    }
                }
            } else {
                noMore = true;
            }
        }
        return toReturn;
    }

    protected Set<Option> getOption(String name)
            throws InterruptedExecutionException{

        assert name.charAt(0) == '-';

        Set<Option> toReturn = new HashSet<>();

        if(name.length() <= 1){
            throw new InterruptedExecutionException(msgs.unknownOption(name));
        }

        if(name.charAt(1) == '-'){
            for(Option ao: availableOptions){
                if(ao.name().equals(name.substring(2, name.length()))){
                    toReturn.add(ao);
                    break;
                }
            }
            if(toReturn.size() == 0){
                throw new InterruptedExecutionException(msgs.unknownOption(name));
            }
            return toReturn;
        }

        StringBuilder b = new StringBuilder();
        for(char a : name.substring(1, name.length()).toCharArray()){
            b.append(a);
            String prop = b.toString();
            for(Option ao: availableOptions){
                if(ao.shortName().equals(prop)){
                    toReturn.add(ao);
                    b.setLength(0);
                    break;
                }
            }
        }
        if(b.length() > 0){
            throw new InterruptedExecutionException(msgs.unknownOption(b.toString()));
        }
        return toReturn;
    }


    protected static boolean ask(String str){
        System.out.print(str+" (y/n) ");
        char answer;
        boolean valid = true;
        do{
            if(!valid){
                System.out.println(Color.RED.applyTo("Please answer y or n\n")+str);
            }
            valid = false;
            String line = scan.nextLine().toLowerCase();
            if(line.length() == 0){
                answer = 'c';
            }else{
                answer = line.charAt(0);
            }
        }while(answer != 'y' && answer != 'n');
        return (answer == 'y');
    }

    protected static String askMore(String str){
        System.out.print(str);
        String response;
        boolean problem = false;
        do{
            response = scan.nextLine();
            problem = false;
            for(char a : response.toCharArray()){
                if (a == '\"'){
                    System.out.println("Invalid input : the character \" is forbidden for obvious reasons.");
                    problem = true;
                    break;
                }
            }
        }while(problem);
        return response;
    }

    public void displayHelp(){
        StringBuilder strB = new StringBuilder();
        strB.append(Color.BOLD.applyTo(format())+(isHidden?(" "+helpInfos.get("_common").get("hidden")):"")+"\n\t"+desc);
        strB.append(Color.BOLD.applyTo("\n\tArguments ")+":\n");
        if(arguments.size() == 0){
            strB.append("\t\tThis command takes no arguments.\n");
        }else{
            for(Pair<String, String> e : arguments){
                strB.append("\t\t"+Color.BOLD.applyTo(e.getA())+" : "+e.getB()+"\n");
            }
        }

        strB.append("\n\t"+Color.BOLD.applyTo("Options")+" :\n");

        if(availableOptions.size() == 0){
            strB.append("\t\tThis command takes no options.\n");
        }else{
            for(Option o : availableOptions){
                strB.append("\t\t"+Color.BOLD.applyTo("-"+o.shortName())+" (--"+o.name()+") : "+o.help()+"\n");
            }
        }

        System.out.println(strB.toString());

    }


    // if command is hidden, it will not be displayed in the listing of commands.
    public boolean isHidden(){
        return isHidden;
    }

    public String toString(){
        return title;
    }

    // not sure if this is used anywhere but oh well.
    public void checkPersonExists(Logger logger, String p) throws InterruptedExecutionException{
        if ( !logger.peopleContains(p) ) {
            throw new InterruptedExecutionException( msgs.personNotFound(p) );
        }
    }


    private static Map<String, Map<String, String>> getHelpInfos(){
        Map<String, Map<String, String>> helpInfos = new HashMap<>();

        try(BufferedReader r = new BufferedReader(new FileReader(".helpInfos"))){
            List<String> l = r.lines().collect(Collectors.toList());
            Iterator<String> it = l.iterator();
            while(it.hasNext()){
                String cmdName = it.next();
                if(cmdName.length() == 0){
                    continue;
                }
                Map<String, String> args = new HashMap<>();
                args.put("desc", it.next());
                String next = it.next();
                while (it.hasNext() && next.length() != 0){
                    String one = it.next();
                    args.put(next, one);
                    next = it.next();
                }
                helpInfos.put(cmdName, args);
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        return helpInfos;
    }
}

