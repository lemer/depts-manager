package ch.migros.commands;

import java.util.List;
import java.util.Map;

import ch.migros.utilitary.FileManager;
import ch.migros.utilitary.Interface;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.InterruptedExecutionException;

public class BackupCmd extends Command{
    private FileManager fM;
    private Interface i;

    public BackupCmd(Interface i){
        super("backup", argsSizeEquals(0,1), "Creates a backup copy of the main file.", true);
        addArgument("<file_name>", "The name of the backup file. This field is optional : if nothing is specified, the backup file will be called .backup");
        this.i = i;
        this.fM = i.getFM();
    }

    @Override
    protected boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException {
        String fileName;
        if(args.size() == 0){
            fileName = ".backup";
        }else if(!args.get(0).equals(i.getFileIn())){
            fileName = args.get(0);
        }else{
            throw new InterruptedExecutionException(msgs.backupOverMainFile(i.getFileIn()));
        }
        fM.save(fileName);
        System.out.println("All informations have been saved in "+fileName);
        return true;
    }

}
