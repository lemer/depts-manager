package ch.migros.commands;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import ch.migros.utilitary.Color;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.InterruptedExecutionException;

public class HelpCmd extends Command {
    public final static String TITLE = "help";
    private final Collection<Command> list;

    private static final Option OPT_h = option("hidden", "H", TITLE);
    private static final Option OPT_d = option("describe", TITLE);
    private static final Option OPT_a = option("alias", TITLE);
    private static final String help = "Displays all available commands. Takes no arguments.";

    public HelpCmd(Collection<Command> l){
        super("help");
        addOption(OPT_a);
        addOption(OPT_d);
        addOption(OPT_h);
        list = l;
    }

    @Override
    public boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException{

        boolean showHidden = askedOptions.containsKey(OPT_h);
        boolean showDesc = askedOptions.containsKey(OPT_d);
        boolean showAliases = askedOptions.containsKey(OPT_a);

        System.out.println("The available commands are :");
        for(Command c : list){
            String desc = "";
            if(showDesc){
                desc = showDesc? String.format("%s", c.desc) : "";
            }
            if(c.isHidden() && showHidden){
                System.out.print(Color.BLUE.applyTo(String.format("\t%-15s%s%n",c.toString(), desc)));
            } else if(c instanceof Alias && showAliases) {
                System.out.print(Color.YELLOW.applyTo(String.format("\t%-15s%s%n",c.toString(), desc)));
            } else if(!c.isHidden() && !(c instanceof Alias)){
                System.out.printf("\t%-15s%s%n",c.toString(), desc);
            }
        }
        System.out.println("You can get more information about a command by typing its name followed by \"-h\"");
        return true;
    }
}
