package ch.migros.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import ch.migros.utilitary.Log;
import ch.migros.utilitary.Logger;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.InterruptedExecutionException;

public class RemoveCmd extends Command{
    private Logger logger;

    public RemoveCmd(Logger logger){
        super("remove", argsSizeEquals(2));
        addArgument("(log|name)");
        addArgument("<which_one>");
        this.logger = logger;
    }

    @Override
    public boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException{
        String option = args.get(0);
        List<String> newArgs = args.subList(1, 2);
        if (option.equals("name")){
            return removeName(newArgs);
        } else if (option.equals("log")) {
            return removeLog(newArgs);
        } else {
            throw new InterruptedExecutionException(msgs.unknownCommand("remove "+option));
        }
    }

    private boolean removeName(List<String> args) throws InterruptedExecutionException{
        String person = args.get(0);
        checkPersonExists(logger, person);

        // Output a confirmation message.
        if (ask("Do you really want to delete every information about " + person + "?\nThis will include all debts towards him,"
                + "\nand he will be replaced by an anonymous person in all logs.") 
                && ask("Really?") 
                && ask("like, REALLY??")) {
            logger.removePerson(person);
            return true;
        } else {
            throw new InterruptedExecutionException(msgs.youFool());
        }
    }

    private boolean removeLog(List<String> args) throws InterruptedExecutionException{
        int index=0;
        try{
            index = Integer.parseInt(args.get(0));
        } catch(NumberFormatException exc) {
            throw new InterruptedExecutionException(msgs.invalidNumberFormat());
        }

        if (index<1 || index> logger.getLogs().size()) {
            throw new InterruptedExecutionException(msgs.noSuchLog(index));
        }

        List<Log> logs = new ArrayList<>(new TreeSet<>(logger.getLogs()));
        Log l = logs.get(index-1);

        if (ask("Remove all informations about this log, and all debts caused by it ?\n\t"+l+"\n")) {
            return logger.removeLog(l);
        }
        return false;
    }

}
