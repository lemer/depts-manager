package ch.migros.commands;

import java.util.List;
import java.util.Map;

import ch.migros.utilitary.Logger;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.InterruptedExecutionException;

public class AddCmd extends Command{
    public final static String TITLE = "add";

    private Logger logger;

    public AddCmd(Logger logger){
        super(TITLE, argsSizeEquals(1));
        addArgument("<name>", TITLE);
        this.logger = logger;
    }

    @Override
    protected boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException {
        if (!logger.addPerson(args.get(0))){
            throw new InterruptedExecutionException(msgs.failToAddPerson(args.get(0)));
        }
        return true;
    }

}
