package ch.migros.commands;

import ch.migros.utilitary.InterruptedExecutionException;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.Person;
import ch.migros.utilitary.Logger;

import java.util.Map;
import java.util.List;

public class WhosNext extends Command{
    private Logger logger;

    public WhosNext(Logger logger){
        super("whosnext", argsSizeEquals(0));
        this.logger = logger;
    }

    protected boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException{
        Person p = null;
        Map<Person, Double> sums = logger.getSumDebts();
        double max = -1.;
        for(Map.Entry<Person, Double> e : sums.entrySet()){
            if (p == null || e.getValue() < max){
                p = e.getKey();
                max = e.getValue();
            }
        }
        if(p == null){
            throw new InterruptedExecutionException("There seems to be no answer to the \"who's next\" question...");
        }else{
            System.out.println(p.name() + " should be paying next.");
        }


        return true;

    }
}
