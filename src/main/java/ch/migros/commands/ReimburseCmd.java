package ch.migros.commands;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import ch.migros.utilitary.Log;
import ch.migros.utilitary.Logger;
import ch.migros.utilitary.Person;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.InterruptedExecutionException;

public class ReimburseCmd extends Command{
    private Logger logger;

    public ReimburseCmd(Logger logger) {
        super("reimburse", argsBetween(2, 4));
        addArgument("<who>");
        addArgument("<toWhom>");
        addArgument("<amout>");
        this.logger = logger;
    }

    @Override
    protected boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException {
        Person p1 = logger.getPerson(args.get(0));
        Person p2 = logger.getPerson(args.get(1));
        // Asks for confirmation
        if(p1.getOwes().containsKey(p2)){
            double amount = (args.size() == 3)? toDouble(args.get(2)) : p1.getOwes().get(p2);
            if (ask(String.format(p1+" owes %.2f to %s.\nDo you want him to pay back %.2f?\n",p1.getOwes().get(p2),p2,amount))){
                if (ask("Save this as a log?")) {
                    logger.log("Reimburement", p1.name(), amount, Arrays.asList(p2.name()));
                    //                    logger.getLogs().add(new Log(p1, amount, Arrays.asList(p2), currentDate, logger.getLogs().size()));
                    System.out.println("Saved.");
                }
            //            p1.getOwes().remove(p2);
                p1.paid(p2, amount);
                logger.setChanged(true);
                return true;
            }
            return false;
            // Or no debts to clean.
        }else{
            System.out.println(msgs.noDebts(p1));
            return true;
        }
    }
}
