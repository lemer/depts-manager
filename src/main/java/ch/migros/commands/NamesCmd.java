package ch.migros.commands;

import java.util.List;
import java.util.Map;

import ch.migros.utilitary.Logger;
import ch.migros.utilitary.Option;

public class NamesCmd extends Command{
    private Logger logger;

    public NamesCmd(Logger logger){
        super("names");
        this.logger = logger;
    }

    @Override
    public boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) {
        // Get and print the people.
        if ( logger.getPeople().isEmpty()) {
            System.out.println(msgs.noNamesToList());
        }
        logger.getPeople().forEach(p -> System.out.println("\t"+p));
        return true;
    }
}
