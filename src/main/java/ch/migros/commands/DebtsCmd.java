package ch.migros.commands;

import java.util.List;
import java.util.Map;

import ch.migros.utilitary.Logger;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.Person;
import ch.migros.utilitary.InterruptedExecutionException;

public class DebtsCmd extends Command{
    public final static String TITLE = "debts";

    private Logger logger;
    private Option OPT_s = option("sum", TITLE);

    public DebtsCmd(Logger logger){
        super(TITLE, argsSizeEquals(0,1));
        addArgument("<name>");
        addOption(OPT_s);
        this.logger = logger;
    }

    @Override
    public boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException{
        if(args.size() == 0){
            return printAll(askedOptions);
        }else{
            return printFor(askedOptions, args.get(0));
        }

    }

    private boolean printAll(Map<Option, List<String>> askedOptions) throws InterruptedExecutionException{
        if(logger.getPeople().size() == 0){
            throw new InterruptedExecutionException(msgs.noNamesToList());
        }

        for(Person p : logger.getPeople()){
            printFor(askedOptions, p.toString());
        }
        return true;
    }

    private boolean printFor(Map<Option, List<String>> askedOptions,String p) throws InterruptedExecutionException{
        return logger.printDebtsFor(logger.getPerson(p), askedOptions.containsKey(OPT_s));
    }
}
