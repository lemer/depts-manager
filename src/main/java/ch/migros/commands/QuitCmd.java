package ch.migros.commands;

import java.util.List;
import java.util.Map;

import ch.migros.utilitary.Interface;
import ch.migros.utilitary.Option;
import ch.migros.utilitary.InterruptedExecutionException;

public class QuitCmd extends Command{

    public static final String TITLE = "quit";
    private Interface i;
    public static Option OPT_d = option("debug", TITLE);

    public QuitCmd(Interface i) {
        super("quit");
        addOption(OPT_d);
        this.i = i;
    }

    @Override
    protected boolean tryToRun(Map<Option, List<String>> askedOptions, List<String> args) throws InterruptedExecutionException {
        if(askedOptions.containsKey(OPT_d)){
            i.debugMode(true);
        }
        i.close();
        return true;
    }
}
